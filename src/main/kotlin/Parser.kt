import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import kotlin.system.exitProcess
import yakstack.yaklib.Obj.DATE_FORMAT

class Parser(parser: ArgParser) {
    val login: String by parser.storing("-l", "--login",
            help = "Initialize client with user info and log in.\n" +
                    "User info passed in as properties file with format:\n" +
                    "$EMAIL=<EMAIL>\n$PASSWORD=<PASSWORD>\n$ENDPOINT=<ENDPOINT>")
                    .default("")

    val register: String by parser.storing("-r", "--register",
            help = "Set up user account and register it to the server.\n" +
                "User info passed in as properties file with format:\n" +
                "$EMAIL=<EMAIL>\n" +
                "$PASSWORD=<PASSWORD>\n" +
                "$FNAME=<FIRST NAME>\n" +
                "$LNAME=<LAST NAME>\n" +
                "$ENDPOINT=<ENDOINT>" +
                "$DEV_NAME=<DEVICE NAME> (optional)\n")
                .default("")

    val push by parser.flagging("-p", "--push",
            help = "Push local commits to Yak server.\n By default, whenever a " +
                "task is added it will try to push all uncommitted tasks to " +
                "the Yak server.")

    val status by parser.flagging("--status", "--tst",
            help = "Check to see if there are any uncommitted tasks.")

    val addTask: List<String> by parser.adding("--add-task", "-a",
            help = "Add a task.\n")
}

class TaskParser(parser: ArgParser) {
    val category: String by parser.positional("CATEGORY",
            help = "Category of task to store")

    val task: String by parser.positional("TASK",
            help = "Name of task being stored")

    val startTime: String by parser.positional("START_TIME",
            help = "Start time of a task in the format $DATE_FORMAT")

    val endTime: String by parser.positional("END_TIME",
            help = "End time of a task in the format $DATE_FORMAT")

    val props: List<String> by parser.positionalList(
            "PROPERTIES",
            sizeRange = 0..Int.MAX_VALUE,
            help = "Extra properties desired to be saved in the format:\n" +
                    "prop1:Property prop2:property"
        )
}

fun String.toDate(): Date {
    try {
        val sd = SimpleDateFormat(DATE_FORMAT)
        return sd.parse(this)
    } catch (e: ParseException) {
        printErr("Malformed date. Must be in format $DATE_FORMAT")
        exitProcess(2)
    }
}

fun List<String>.toPropertyMap(): Map<String, String> {
    val propMap = mutableMapOf<String, String>()
    this.forEach {
        val propVal = it.split(":").map { it.trim() }
        if (propVal.size < 2) {
            throw RuntimeException("Malformed property: ${propVal[0]} doesn't have a value")
        }
        propMap[propVal[0]] = propVal[1]
    }
    return propMap
}
