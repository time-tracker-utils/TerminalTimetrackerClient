# Exit Codes

Each of the exit codes stand for a different error.

If you're using time-client from another program, you can use these
codes to determine what's happening without parsing text.

* 0 = Executed fine
* 1 = Malformed command
e.g. Not all required arguments were passed in
* 2 = Malformed argument syntax
e.g. Date format or secrets were not correct
* 3 = Network/server error
* 4 = Invalid attempt to log in
* 5 = Attempt to do an action a user must be logged in for
* 254 = Unknown error
