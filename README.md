# TIMETRACKER-CLIENT (ALPHA PROJECT)

This terminal timetracker-client is an alpha app client for my time
tracking API. The API is in development (just like this project) so
don't ask yet.

If you'd like to see the planning API docs, you can check [here](https://docs.google.com/document/d/1BfvIqC83ySDgAiyZlAbxV5Biru6FMrIaVt5OKAlmIFc/edit?usp=sharing).
Feel free to comment :)

For now, here's some documentation on the timetracker-client.

# timetracker-client

Want it NOOOOW? Grab the latest release in the `tags`.
FYI, you need to have Java installed.

time-tracker client is a terminal application with args that work
the way you'd expect.

You can even register and log in straight from the client!

```bash
$ timetracker-client -h
usage: [-h] [-l LOGIN] [-r REGISTER] [--verbose] [--quiet] [--status]
       [--add-task ADD_TASK]...

optional arguments:
  -h, --help             show this help message and exit

  -l LOGIN,              Initialize client with user info and log in. User
  --login LOGIN          info passed in as properties file with format:
                         email=<EMAIL> password=<PASSWORD>

  -r REGISTER,           Set up user account and register it to the server.
  --register REGISTER    User info passed in as properties file with format:
                         email=<EMAIL> password=<PASSWORD> fname=<FIRST NAME>
                         lname=<LAST NAME>

  --verbose, -v          Add verbosity to output.

  --quiet, -q            Silence all output.

  --status, --tst        Check to see if there are any uncommitted tasks.

  --add-task ADD_TASK,   Add a task.
  -a ADD_TASK
```

The client must be intitialized with a property file.

All you need is a username, password, and the endpoint of your time tracker
server, and you're ready to go!

```
email=<EMAIL>
password=<PASSWORD>
endpoint=<ENDPOINT>

```

If you want to register, you also need to add fname (first name) and lname
(last name) properties in your properties file as well.

--

Tasks are saved in a commits file before they are pushed to your server to
ensure no data is lost if running without internet. This is all transparent
to the user, but --status or --tst is given as a flag if you'd like to see
uncommitted tasks.

## Adding a task

```bash
$ timetracker-client --add-task --help
usage: [-h] TASK CATEGORY START_TIME END_TIME [PROPERTIES]...

optional arguments:
  -h, --help   show this help message and exit


positional arguments:
  TASK         Name of task being stored

  CATEGORY     Category of task to store

  START_TIME   Start time of a task in the format yyyy/mm/dd-hh:mm

  END_TIME     End time of a task in the format yyyy/mm/dd-hh:mm

  PROPERTIES   Extra properties desired to be saved in the format:
               prop1:Property prop2:property



```

In addition to the required fields, task, category, start_time and end_time,
you can also add as many extra properties as you'd like with the syntax:
`prop1:property prop2:property`

# Building

Want the LATEST code ON YOUR MACHINE RIGHT NOW?

timetracker-client is a Kotlin app, so building it is one command.

Make sure you have Java installed, although this requirement may go away
in the future.

Clone the repo:

`git clone https://gitlab.com/time-tracker-utils/TerminalTimetrackerClient`

Go into the dir:

`cd TerminalTimeTrackerClient`

Build it:

`./gradlew distZip`

DONE!

The zip will be in the folder:

`/build/distributions`

To run it, unzip that folder, and then `cd` to the bin folder.

You're done!

# Contributing

All code is expected to pass through the default [ktlint](https://github.com/shyiko/ktlint) settings.
If you're running Arch Linux, you can install it via AUR.

Otherwise, check the install instructions for your operating system!
